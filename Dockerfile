from python:alpine

RUN pip3 install requests bs4 PyYAML

COPY app /app

RUN python3 app/poll-n-push.py test

ENTRYPOINT ["python3", "/app/poll-n-push.py"]
