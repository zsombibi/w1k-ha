note: this solution is superseeded by the HACS integration: https://github.com/ZsBT/hass-w1000-portal

# w1k-HA

Pulls data from a W1000 portal report and sends its last value to Home Assistant via its REST API.
You need to have some Docker knowledge.
See some screenshots at the Wiki page.

## Prepare data sources

- Register and log in to your provider's portal (e.g. https://energia.eon-hungaria.hu/W1000 in Hungary)
- Create a new Workarea (use any name)
- Add a report to your workarea. Remember this *report name*, it will be your new sensor in Home Assistant.
- Add exactly one *curve* (e.g. HU0XXXXX-1:1.8.0*0 is the total power consumption counter of your electricity meter) to your report. I recommend setting a 3-day interval.

You can create more reports on any workareas, just make sure you have exactly one curve at a report.

## Allow Home Assistant to receive data

- In your Home Assistant, under your profile -> Long-Lived Access Tokens , create a new one for this purpose.

## Create a docker container

This repository builds a docker image at Docker Hub, so the easiest way is to use it:

`$ docker run --rm -it -e W1K_USER=xxx -e W1K_PASS=yyy ...   --name w1k-ha  buffertly/w1k-ha`

Of course there are some mandatory environment variables. Double-check the values are present and valid, as the code does not handle errors quite user-friendly.

```
W1K_USER        loginname or email for the W1000 web portal
W1K_PASS        password for the W1000 web portal
W1K_REPORTS     report name (or names, separated by commas)
W1K_HA_URL      your Home Assistant URL
W1K_HA_TOKEN    the Long-Lived Access Token you generated
```

Optional:
```
W1K_URL         W1000 web portal URL. Defaults to https://energia.eon-hungaria.hu/W1000
```

If everything goes well, the container logs what has been done:

```
HA sending result: <Response [200]> {'attributes': {'friendly_name': 'w1000 óraállás', 'state_class': 'measurement', 'unit_of_measurement': 'kWh', 'unique_id': 'w1000_ralls', 'device_class': 'energy'}, 'state': 460.0}
```

In Home Assistant, you will find new sensor entities for all reports. Please note, non-ascii characters are truncated in the entity names.

The container exits immediately, so you have to create or re-start it periodically.
