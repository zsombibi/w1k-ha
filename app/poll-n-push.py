#!/bin/env python3
#

from w1000 import W1000_client, HA_client
from os import getenv
from sys import argv, exit


def main():
  w1k = W1000_client( getenv("W1K_USER"), getenv("W1K_PASS"), getenv("W1K_URL",'https://energia.eon-hungaria.hu/W1000') )

  HA = HA_client( getenv("W1K_HA_TOKEN"), getenv("W1K_HA_URL") )  

  for report in getenv("W1K_REPORTS").split(","):
    safename = report.encode("ascii", "ignore").decode().replace(" ","_")
    lastdata = w1k.read_reportname( report )[0]
    payload = {"attributes":{
      "friendly_name":	"w1000 "+report, 
      "state_class":	"measurement",
      "unit_of_measurement": lastdata['unit'],
      "unique_id":	"w1000_"+safename,
      "curve":		lastdata['curve'],
      "report":		report,
      "time_generated":	lastdata['last_time'],
      "device_class":	"energy"},
      "state": lastdata['last_value'] 
    }
    
    res = HA.sendstate("sensor.w1000_"+safename, payload)
    print("HA sending result:", payload, res, res.text)
  
  return 0	# number of errors
  

if __name__ == '__main__':
  if 'test' in argv:
    print( "syntax check OK" )
    exit(0)
  
  exit( main() )
  