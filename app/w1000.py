#
#	W1000 class
#	https://github.com/ZsBT/w1000-ha
#
#	reads the last value of a report
#	use exactly one curve on a report
#	
#	login/session by https://github.com/amargo/eon-mqtt	thanks!
#

from bs4 import BeautifulSoup
import requests, yaml, re, urllib3
from datetime import datetime, timedelta

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class W1000_client:

    def __init__(self, username, password, url):
        self.username = username
        self.password = password
        
        if not username:
            raise ValueError("missing username")
        
        if not password:
            raise ValueError("missing password")
            
        if not url:
            raise ValueError("missing URL")
            
        self.account_url = url+"/Account/Login"
        self.profile_data_url = url + "/ProfileData/ProfileData"
        self.lastlogin = None



    def get_verificationtoken(self,content):
        toke = content.find('input', {'name': '__RequestVerificationToken'})
        return toke.get('value')


    def login(self):
        try:
            self.session = requests.Session()
            content = self.session.get(self.account_url, verify = False)
            index_content = BeautifulSoup(content.content, "html.parser")
            self.request_verification_token = self.get_verificationtoken(index_content)

            payload = {
                "UserName": self.username,
                "Password": self.password,
                "__RequestVerificationToken": self.request_verification_token
            }

            header = {"Content-Type": "application/x-www-form-urlencoded"}
            resp = self.session.post(self.account_url, data=payload, headers=header, verify = False)
            match = re.findall( r'W1000.start\((.+)sessionTimeout', resp.text.replace("\n", " ") )
            
            if len(match)==0:
                raise ValueError("Login failed")
            
            respob = yaml.safe_load(match[0]+"}")
            self.currentUser = respob['currentUser']
            self.workareas = respob['workareas']
            self.lastlogin = datetime.utcnow()
            
        except Exception as ex:
            availability = 'Offline'
            print(datetime.now(), "Error retrive data from {0}.".format(str(ex)))
            
    
    
    def read_reportname(self, reportname: str):
        if not self.lastlogin or self.lastlogin + timedelta(hours=1) < datetime.utcnow():
            self.login()
            
        for workarea in self.workareas:
            for window in workarea['windows']:
                if window['name'] == reportname:
                    return self.read_reportid( int(window['reportid']) )
        
        print("report "+reportname+" not found")
        return None
        

    def read_reportid(self, reportid: int):
        now = datetime.utcnow()

        if not self.lastlogin or self.lastlogin + timedelta(hours=1) < now:
            self.login()
        
        since = (now + timedelta(days=-2)).strftime("%Y-%m-%dT00:00:00")
        until = (now + timedelta(days=0 )).strftime("%Y-%m-%dT%H:00:00")
        
        params = {
            "page": 1,"perPage": 96*3,
            "reportId": reportid,
            "since": since,
            "until": until,
            "_": (now - timedelta(hours=3)).strftime("%s557")
        }
        
#        print( params )
        
        resp = self.session.get(self.profile_data_url, params=params)
        
        if resp.status_code == 200:
            jsonResponse = resp.json()
#            print(datetime.now(), jsonResponse)
            lastvalue = None
            unit = None
            lasttime = None
            ret = []
            for window in jsonResponse:
#                print("report "+window['name']+":")
                unit = window['unit']
                for data in window['data']:
                    if data['value'] > 0:
                        lastvalue = round(data['value'],1)
                        lasttime = data['time']
#                        print( "\t",data['time'], lastvalue, window['unit'] )
                ret.append( {'curve':window['name'], 'last_value':lastvalue, 'unit':window['unit'], 'last_time':lasttime} )
        else:
            print("error http "+str(resp.status_code) )
            print( resp.text )

        return ret
        




#
#	dummy Home Assistant client to send data to its API
#

class HA_client:

  def __init__(self, hatoken, baseurl="http://localhost:8123"):
    self.hatoken = hatoken
    self.baseurl = baseurl
    
    if not hatoken:
        raise ValueError("no Home Assistant token received")
  
    if not baseurl:
        raise ValueError("no Home Assistant URL received")
  
  def sendstate(self, entity_id, payload ):
    return requests.post( self.baseurl+"/api/states/"+entity_id, 
        headers={'Authorization': 'Bearer '+self.hatoken}, 
        json=payload 
    )

